﻿using FrmUsuario.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrmUsuario.model
{
    class UsuarioModel
    {
        private static List<Usuario> usuarios = new List<Usuario>();


        public static List<Usuario> GetUsuario()
        {
            return usuarios;
        }

        public static void Populate()
        {
            Usuario[] usu =
            {
                new Usuario(1,"armando_paredes","conladrillosybloques","conladrillosybloques",
                "armandoparedes@gmail.com","armandoparedes@gmail.com","2222-1111","2222-1111",3),
                new Usuario(2,"soila_vaca","delagranja","delagranja",
                "soilavaca@yahoo.com","soilavaca@yahoo.com","2222-3333","2222-3333",3)

            };

            usuarios = usu.ToList();
        }
    }
}
