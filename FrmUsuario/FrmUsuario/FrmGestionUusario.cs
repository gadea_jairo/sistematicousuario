﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmUsuario
{
    public partial class FrmGestionUusario : Form
    {

        private DataSet dsUsuario;
        private BindingSource bsUsuario;



        public DataSet DsUsuario { set { dsUsuario = value; } }

        public FrmGestionUusario()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsUsuario.DataSource = dsUsuario;
            bsUsuario.DataMember = dsUsuario.Tables["Usuario"].TableName;
            dgvusuario.DataSource = bsUsuario;
            dgvusuario.AutoGenerateColumns = true;
        }

        private void btnuevo_Click(object sender, EventArgs e)
        {
            FrmUsuario usf = new FrmUsuario();
            usf.TblUsuario = dsUsuario.Tables["Usuario"];
            usf.DsUsuario = dsUsuario;
            usf.ShowDialog();
        }
    }
}
