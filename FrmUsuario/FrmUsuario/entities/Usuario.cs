﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrmUsuario.entities
{
    class Usuario
    {
        private int id;
        private string nombre;
        private string contraseña;
        private string ccontraseña;
        private string correo;
        private string ccorreo;
        private string telefono;
        private string ctelefono;
        private int canterrores;

        public Usuario(int id, string nombre, string contraseña, string ccontraseña, string correo, string ccorreo, string telefono, string ctelefono, int canterrores)
        {
            this.id = id;
            this.nombre = nombre;
            this.contraseña = contraseña;
            this.ccontraseña = ccontraseña;
            this.correo = correo;
            this.ccorreo = ccorreo;
            this.telefono = telefono;
            this.ctelefono = ctelefono;
            this.canterrores = canterrores;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }

            set
            {
                contraseña = value;
            }
        }

        public string Ccontraseña
        {
            get
            {
                return ccontraseña;
            }

            set
            {
                ccontraseña = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Ccorreo
        {
            get
            {
                return ccorreo;
            }

            set
            {
                ccorreo = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Ctelefono
        {
            get
            {
                return ctelefono;
            }

            set
            {
                ctelefono = value;
            }
        }

        public int Canterrores
        {
            get
            {
                return canterrores;
            }

            set
            {
                canterrores = value;
            }
        }
    }
}
