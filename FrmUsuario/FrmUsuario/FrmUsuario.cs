﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmUsuario
{
    public partial class FrmUsuario : Form
    {

        private DataTable tblUsuario;
        private DataSet dsUsuario;
        private BindingSource bsUsuario;
        private DataRow drUsuario;
        public FrmUsuario()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();
        }

        public DataTable TblUsuario { set { tblUsuario = value; } }
        public DataSet DsUsuario { set { dsUsuario = value; } }

        public DataRow DrUsuario
        {
            set
            {
                drUsuario = value;
                txtid.Text = drUsuario["Id"].ToString();
                txtusuario.Text = drUsuario["Usuario"].ToString();
                txtcontraseña.Text = drUsuario["Contraseña"].ToString();
                txtccontraseña.Text = drUsuario["Confirmar Contraseña"].ToString();
                txtcorreo.Text = drUsuario["Correo"].ToString();
                txtccorreo.Text = drUsuario["Confirmar Correo"].ToString();
                txttelefono.Text = drUsuario["Telefono"].ToString();
                txtctelefono.Text = drUsuario["Confirmar Telefono"].ToString();
               
            }

        }

        private void FrmUsuario_Load(object sender, EventArgs e)
        {

        }

        private void btnguardar_Click(object sender, EventArgs e)
        {

        }
    }
}
